import React from "react";
import { Row } from "react-bootstrap";
import UserItem from './users-item';

function UserList({ users }) {
    return (
        <Row className="gy-2">
            {users.map((user) => (
            <UserItem user={user} key={user.id} />
            ))}
        </Row>
    );
}


export default UserList;