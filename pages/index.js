import Layout from "../layouts/Layout";
import UserList from "../components/users-list";
import { host } from "../configs";
import { Row, Col, Form } from "react-bootstrap";
import React from "react";

export default function Home({ usersApi }) {
  const [users, setUsers] = React.useState(usersApi.data);

  // Menggunakan serverSideRendering
  // const [users, setUsers] = React.useState([]);

  // React.userEffect (() => {
  // (async function() {
  //   const response = await host.get('users?page=2');
  //   const { data } = response.data;
  //   setUsers(data);
  // })();
  // }), []);
  const funcFilter = (e) => {
    const value = e.target.value;

    const filterData = users.filter((user) => {
      return (
        user.first_name.search(value) != -1 ||
        user.last_name.search(value) != -1
      );
    });

    if (value.length == 0 || value.trim() == "") {
      (async function () {
        const res = await host.get("users?page=2");
        const { data } = res.data;
        setUsers(data);
      })();
    }

    setUsers(filterData);
  };

  // const data = users.data;
  return (
    <div>
      <Layout>
        <Row className="justify-content-md-end mb-3">
          <Col md={5}>
            <Form.Control
              type="text"
              placeholder="Search users ..."
              onChange={funcFilter}
            />
          </Col>
        </Row>
        <UserList users={users} />
      </Layout>
    </div>
  );
}

export async function getServerSideProps(context) {
  const res = await host.get("users?page=2");
  const data = res.data;

  return {
    props: { usersApi: data }, // will be passed to the page component as props
  };
}


